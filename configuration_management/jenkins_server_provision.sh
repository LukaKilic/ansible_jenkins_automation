#!/bin/bash

echo "----- Setup ansible configuration. -----"
sed "s/{SERVER_IP}/${SERVER}/g" /etc/ansible/hosts.template > /etc/ansible/hosts
sed "s/{SERVER_USER}/${SERVER_USER}/g" /etc/ansible/group_vars/servers.template > /etc/ansible/group_vars/servers

echo "----- Running django tests. -----"
rm -rf env
virtualenv -p /usr/bin/python2.7 env
. env/bin/activate
cd notejam
pip install -r requirements.txt
python manage.py test --noinput --settings=notejam.settings --verbosity=2

if [[ $? -ne 0 ]]
then
	echo "Tests failed, marking build as failed."
    exit 1
fi

echo "----- Provisioning the remote server. -----"
cd ../

if [[ ${ONLY_CODE_DEPLOY} = true ]]
then
	TAGS="--tags code_deploy"
else
	TAGS=""
fi

sed "s/{SERVER_IP}/${SERVER}/g" configuration_management/notejam_nginx.conf.template > configuration_management/notejam_nginx.conf
ansible-playbook configuration_management/ansible_playbook.yml ${TAGS}
